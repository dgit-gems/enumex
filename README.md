# Enumex
Extensions for rails enums to add display names to be used in views/API responses

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'enumex'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install enumex
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

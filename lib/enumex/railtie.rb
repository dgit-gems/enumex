module Enumex
  class Railtie < ::Rails::Railtie
    initializer 'enumex' do
      ActiveSupport.on_load :active_record do
        ::ActiveRecord::Base.extend Enumex::ActiveRecordExtension
      end
    end
  end
end

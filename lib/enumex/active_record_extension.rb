module Enumex
  module ActiveRecordExtension
    def self.extended(base) # :nodoc:
      base.class_attribute(:defined_enums, instance_writer: false)
      base.class_attribute(:defined_enumexes, instance_writer: false)
      base.defined_enums = {}
      base.defined_enumexes = {}
    end

    def inherited(base) # :nodoc:
      base.defined_enums = defined_enums.deep_dup
      base.defined_enumexes = defined_enumexes.deep_dup
      super
    end

    class EnumexType < ActiveRecord::Type::Value # :nodoc:
      delegate :type, to: :subtype

      def initialize(name, composite_mapping, subtype)
        @name = name

        @mapping = ActiveSupport::HashWithIndifferentAccess.new(composite_mapping.map{|k, v| [k, v[0]]}.to_h)
        @mappingex = ActiveSupport::HashWithIndifferentAccess.new(composite_mapping.map{|k, v| [k, v[1]]}.to_h)

        @subtype = subtype
      end

      def cast(value)
        return if value.blank?

        if mapping.key?(value)
          value.to_s
        elsif mapping.value?(value)
          mapping.key(value)
        elsif mappingex.value?(value)
          mappingex.key(value)
        else
          assert_valid_value(value)
        end
      end

      def deserialize(value)
        return if value.nil?

        deserialized_value = subtype.deserialize(value)

        if mapping.value?(deserialized_value)
          mapping.key(deserialized_value)
        elsif mappingex.value?(deserialized_value)
          mappingex.key(deserialized_value)
        end
      end

      def serialize(value)
        if mapping.key?(value)
          mapping.fetch(value, value)
        elsif mappingex.value?(value)
          mapping.fetch(mappingex.key(value), value)
        else
          value
        end
      end

      def assert_valid_value(value)
        # rubocop:disable Style/GuardClause
        unless value.blank? || mapping.key?(value) || mapping.value?(value) || mappingex.value?(value)
          raise ArgumentError, "'#{value}' is not a valid #{name}"
        end
        # rubocop:enable Style/GuardClause
      end

      private

      attr_reader :name, :mapping, :subtype, :mappingex

    end

    # rubocop:disable Metrics/MethodLength, Metrics/PerceivedComplexity
    def enumex(definitions)
      klass = self
      enum_prefix = definitions.delete(:_prefix)
      enum_suffix = definitions.delete(:_suffix)
      enum_scopes = definitions.delete(:_scopes)
      definitions.each do |name, values|
        # statuses = { }
        enum_values = ActiveSupport::HashWithIndifferentAccess.new
        enumex_values = ActiveSupport::HashWithIndifferentAccess.new
        values = ActiveSupport::HashWithIndifferentAccess.new(values)
        name = name.to_s

        # def self.statuses() statuses end
        detect_enum_conflict!(name, name.pluralize, true)
        singleton_class.define_method(name.pluralize) { enum_values }
        defined_enums[name] = enum_values

        detect_enum_conflict!(name, "enumex_#{name.pluralize}", true)
        singleton_class.define_method("enumex_#{name.pluralize}") { enumex_values }
        defined_enumexes[name] = enumex_values

        detect_enum_conflict!(name, name)
        detect_enum_conflict!(name, "#{name}=")

        # def enumex_status_display() User.enumex_statuses[:active] end
        detect_enum_conflict!(name, "enumex_#{name}_display", true)
        define_method("enumex_#{name}_display") { klass.send("enumex_#{name.to_s.pluralize}")[self[name]] }

        attr = attribute_alias?(name) ? attribute_alias(name) : name
        decorate_attribute_type(attr, :enumex) do |subtype|
          EnumexType.new(attr, values, subtype)
        end

        _enum_methods_module.module_eval do
          pairs = values.respond_to?(:each_pair) ? values.each_pair : values.each_with_index
          pairs.each do |label, db_display|
            i, display_name = db_display

            if enum_prefix == true
              prefix = "#{name}_"
            elsif enum_prefix
              prefix = "#{enum_prefix}_"
            end
            if enum_suffix == true
              suffix = "_#{name}"
            elsif enum_suffix
              suffix = "_#{enum_suffix}"
            end

            value_method_name = "#{prefix}#{label}#{suffix}"
            enum_values[label] = i
            enumex_values[label] = display_name

            # def active?() status == 0 end
            klass.send(:detect_enum_conflict!, name, "#{value_method_name}?")
            define_method("#{value_method_name}?") { self[attr] == value.to_s }

            # def active!() update! status: :active end
            klass.send(:detect_enum_conflict!, name, "#{value_method_name}!")
            define_method("#{value_method_name}!") { update!(attr => value) }

            # scope :active, -> { where status: 0 }
            if enum_scopes != false
              klass.send(:detect_negative_condition!, value_method_name)

              klass.send(:detect_enum_conflict!, name, value_method_name, true)
              klass.scope value_method_name, -> { where(attr => value) }

              klass.send(:detect_enum_conflict!, name, "not_#{value_method_name}", true)
              klass.scope "not_#{value_method_name}", -> { where.not(attr => value) }
            end

          end
        end
        enum_values.freeze
        enumex_values.freeze
      end
      # rubocop:enable Metrics/MethodLength, Metrics/PerceivedComplexity
    end

    private

    def _enum_methods_module
      @_enum_methods_module ||= begin
        mod = Module.new
        include mod
        mod
      end
    end

    ENUM_CONFLICT_MESSAGE = \
      "You tried to define an enum named \"%<enum>\" on the model \"%<klass>\", but " \
      "this will generate a %<type> method \"%<method>\", which is already defined " \
      "by %<source>.".freeze
    private_constant :ENUM_CONFLICT_MESSAGE

    def detect_enum_conflict!(enum_name, method_name, klass_method = false)
      if klass_method
        raise_conflict_error(enum_name, method_name, type: 'class') if dangerous_class_method?(method_name)
        raise_conflict_error(enum_name, method_name, type: 'class', source: ActiveRecord::Relation.name) if method_defined_within?(method_name, ActiveRecord::Relation)
      else
        raise_conflict_error(enum_name, method_name) if dangerous_attribute_method?(method_name)
        if method_defined_within?(method_name, _enum_methods_module, Module)
          raise_conflict_error(
            enum_name,
            method_name,
            source: 'another enum'
          )
        end
      end
    end

    def raise_conflict_error(enum_name, method_name, type: 'instance', source: 'Active Record')
      raise ArgumentError, format(
        ENUM_CONFLICT_MESSAGE,
        enum: enum_name,
        klass: self.name,
        type: type,
        method: method_name,
        source: source
      )
    end

    def detect_negative_condition!(method_name)
      if method_name.start_with?("not_") && logger
        logger.warn "An enum element in #{self.name} uses the prefix 'not_'." \
          " This will cause a conflict with auto generated negative scopes."
      end
    end
  end
end
